﻿namespace TestApiApp
{
    #region usings

    using System;
    using System.Collections.Generic;
    using System.Dynamic;
    using System.Net;
    using System.Text;
    using System.Threading;
    using System.IO;
    using System.Reflection;

    using WebSocketSharp;
    using WebSocketSharp.Net;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    using uPLibrary.Networking.M2Mqtt;
    using uPLibrary.Networking.M2Mqtt.Messages;

    using NJsonSchema;
    using NJsonSchema.CodeGeneration.CSharp;

    #endregion

    class Program
    {
        /// <summary>
        /// MQTT and WebSocket timeout [ms]
        /// </summary>
        const int Timeout = 5 * 1000;

        /// <summary>
        /// GW IP address
        /// </summary>
        //const string IqrfGwDeamonIP = "192.168.1.101";
        //const string IqrfGwDeamonIP = "127.0.0.1";
	    //const string IqrfGwDeamonIP = "172.17.0.1";

        /// <summary>
        /// Log transaction
        /// </summary>
        /// <param name="mType"></param>
        /// <param name="data"></param>
        private static void logTransaction(string mType, string prefix, string data)
        {
            System.IO.File.AppendAllText(@"logs/" + mType + ".log", prefix + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.FFF") + "\r\n" + data + "\r\n");
        }

        /// <summary>
        /// Log response
        /// </summary>
        /// <param name="mType"></param>
        /// <param name="data"></param>
        private static void logResponse(string mType, string data)
        {
            System.IO.File.WriteAllText(@"tests/" + mType + "-response-1-0-0.json", data);
        }

        /// <summary>
        /// Make example file
        /// </summary>
        /// <param name="mType"></param>
        /// <param name="direction"></param>
        /// <param name="data"></param>
        private static void logExample(string mType, string direction, string data)
        {
            System.IO.File.WriteAllText(@"logs/" + mType + "-" + direction + "-1-0-0-example.json", data);
        }

        /// <summary>
        /// WebSocket request to iqrf-daemon
        /// </summary>
        /// <param name="url"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        private static List<string> WebSocketRequest(string iqrfGwDaemonIP, dynamic request, int timeoutMs)
        {
            // Create WebSocket client
            WebSocket ws = new WebSocket("ws://" + iqrfGwDaemonIP + ":1338");
            try
            {
                int timeout = timeoutMs / 100;
                List<string> response = new List<string>();

                // Set MsgId
                string mtype = "";
                string strRequest = "";
                if (request is string)
                {
                    // Get mType
                    int startIndex = request.IndexOf("\"mType\": \"", 0) + "\"mType\": \"".Length;
                    int length = request.IndexOf("\"", startIndex) - startIndex;
                    mtype = request.Substring(startIndex, length);
                    strRequest = request;
                }
                else
                {
                    request.Data.MsgId = Guid.NewGuid().ToString();
                    strRequest = ((object)request).JsonSerialize();
                    mtype = request.MType.ToString();
                    mtype = Char.ToLowerInvariant(mtype[0]) + mtype.Substring(1);
                }
                // Log the request
                logTransaction(mtype, "==================================\r\nSending to WebSocket: ", strRequest);
                logExample(mtype, "request", strRequest);

                // Add event to receive MQTT message
                ws.OnMessage += (sender, e) =>
                {
                    // Log response
                    logTransaction(mtype, "Received from WebSocket: ", e.Data);
                    logExample(mtype, "response", e.Data);
                    logResponse(mtype, e.Data);
                    response.Add(e.Data);
                    // Preset timeout for next possible response to 0.5 sec (5 * 100 ms)
                    timeout = 500 / 100;
                };

                // Connect to iqrf-daemon
                ws.Connect();
                // Send the request
                ws.Send(strRequest);
                // Wait for response 
                do
                {
                    Thread.Sleep(100);
                } while (--timeout != 0);

                // Log end of the transction
                logTransaction(mtype, "", "==================================\r\n");
                return response;
            }
            finally
            {
                ws.Close();
            }
        }

        /// <summary>
        /// MQTT request to iqrf-daemon
        /// </summary>
        /// <param name="iqrfGwDaemon"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        private static List<string> MQTTRequest(string iqrfGwDaemon, dynamic request, int timeoutMs)
        {
            // Create MQTT client
            MqttClient mqttClient = new MqttClient(iqrfGwDaemon);
            // Connect to the MQTT broker
            mqttClient.Connect(Guid.NewGuid().ToString());
            try
            {
                // Initial timeout
                int timeout = timeoutMs / 100;
                List<string> response = new List<string>();

                // Set MsgId
                string mtype = "";
                string strRequest = "";
                if (request is string)
                {
                    // Get mType
                    int startIndex = request.IndexOf("\"mType\": \"", 0) + "\"mType\": \"".Length;
                    int length = request.IndexOf("\"", startIndex) - startIndex;
                    mtype = request.Substring(startIndex, length);
                    strRequest = request;
                }
                else
                {
                    request.Data.MsgId = Guid.NewGuid().ToString();
                    strRequest = ((object)request).JsonSerialize();
                    mtype = request.MType.ToString();
                    mtype = Char.ToLowerInvariant(mtype[0]) + mtype.Substring(1);
                }

                // Log the request
                logTransaction(mtype, "==================================\r\nSending to MQTT: ", strRequest);
                logExample(mtype, "request", strRequest);

                // Add event to receive MQTT message
                mqttClient.MqttMsgPublishReceived += (sender, e) =>
                {
                    // Log response
                    string rsp = Encoding.UTF8.GetString(e.Message);
                    logTransaction(mtype, "Received from MQTT: ", rsp);
                    logExample(mtype, "response", rsp);
                    logResponse(mtype, rsp);
                    response.Add(rsp);
                    // Preset timeout for next possible response to 0.5 sec (5 * 100 ms)
                    timeout = 500 / 100;
                };

                // Subscribe IQRF/DpaResponse at MQTT broker
                mqttClient.Subscribe(new string[] { "Iqrf/DpaResponse" }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });
                mqttClient.Publish("Iqrf/DpaRequest", Encoding.UTF8.GetBytes(strRequest), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, true);
                // Wait for response 
                do
                {
                    Thread.Sleep(100);
                } while (--timeout != 0);

                // Log end of the transction
                logTransaction(mtype, "", "==================================\r\n");
                return response;
            }
            finally
            {
                mqttClient.Disconnect();
            }
        }

        /// <summary>
        /// Automated testing daemon API
        /// </summary>
        /// <param name="dirJsonRequest"></param>
        /// <param name="Messaging"></param>
        private static void testAPI(string dirJsonRequest, string ipAddressGw, Func<string, string, int, List<string>> sendRequest)
        {
            Console.WriteLine("Starting API test ...");
            // Test all requests contained in the directory dirJsonRequest    
            try
            {
                // Check dirJsonRequest
                if (!Directory.Exists(dirJsonRequest))
                {
                    throw new Exception("Directory " + dirJsonRequest + " not found!");
                }
                DirectoryInfo d = new DirectoryInfo(dirJsonRequest);

                FileInfo[] Files = d.GetFiles("*-request-*.json"); //Getting Text files

                foreach (FileInfo file in Files)
                {
                    // Read the file
                    string strRequest = File.ReadAllText(file.FullName);
                    Console.WriteLine(file.Name);
                    // Send the request to IQRF GW Daemon
                    //Console.WriteLine(strRequest);
                    sendRequest(ipAddressGw, strRequest, Timeout);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.Write("Completed.");
        }

        /// <summary>
        /// Main function
        /// </summary>
        static void Main(string[] args)
        {
            // Test if input arguments were supplied:
            if (args.Length == 0 || args.Length > 1 )
            {
                System.Console.WriteLine("Please enter a WebSocket server IP address.");
                System.Console.WriteLine("Usage: test-api-app <string>");
                return;
            }

            string IqrfGwDeamonIP = args[0];

            // Automated test of predefined APIs
            //testAPI(@"../../tests/", MQTTRequest);
            testAPI(@"tests", IqrfGwDeamonIP, WebSocketRequest);
        }
    }

    #region Extensions
    public static class Extensions
    {
        /// <summary>
        /// Extension of object type for easy serialization to JSON
        /// </summary>
        public static string JsonSerialize(this object obj) => JsonConvert.SerializeObject(obj, Formatting.Indented);

        /// <summary>
        /// Extension of string type for easy deserialization from JSON
        /// </summary>
        public static dynamic JsonDeserialize(this string json) => JsonConvert.DeserializeObject<ExpandoObject>(json, new ExpandoObjectConverter());

        /// <summary>
        /// Extension of string type for easy List deserialization from JSON
        /// </summary>
        public static List<ExpandoObject> JsonDeserializeList(this string json) => JsonConvert.DeserializeObject<List<ExpandoObject>>(json, new ExpandoObjectConverter());
    }
    #endregion
}
